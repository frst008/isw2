@extends('layouts.master')

@section('content')

    <h1>Course</h1>
    <div class="table-responsive">
        <table class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>ID.</th> <th>Nombre</th><th>Codigo</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ $course->id }}</td> <td> {{ $course->nombre }} </td><td> {{ $course->codigo }} </td>
                </tr>
            </tbody>    
        </table>
    </div>

@endsection