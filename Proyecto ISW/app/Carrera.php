<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CarreraController
 *
 * @author  The scaffold-interface created at 2016-03-08 07:57:07pm
 * @link  https://github.com/amranidev/scaffold-interfac
 */
class Carrera extends Model
{

    public $timestamps = false;

    protected $table = 'carreras';

	
}
