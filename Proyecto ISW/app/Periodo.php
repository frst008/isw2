<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PeriodoController
 *
 * @author  The scaffold-interface created at 2016-03-08 07:57:41pm
 * @link  https://github.com/amranidev/scaffold-interfac
 */
class Periodo extends Model
{

    public $timestamps = false;

    protected $table = 'periodos';

	
}
