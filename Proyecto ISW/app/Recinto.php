<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class RecintoController
 *
 * @author  The scaffold-interface created at 2016-03-08 08:06:03pm
 * @link  https://github.com/amranidev/scaffold-interfac
 */
class Recinto extends Model
{

    public $timestamps = false;

    protected $table = 'recintos';

	
}
