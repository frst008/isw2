<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class HorarioController
 *
 * @author  The scaffold-interface created at 2016-03-08 07:58:32pm
 * @link  https://github.com/amranidev/scaffold-interfac
 */
class Horario extends Model
{

    public $timestamps = false;

    protected $table = 'horarios';

	
}
