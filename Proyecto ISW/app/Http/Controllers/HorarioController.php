<?php
namespace App\Http\Controllers;

use Request;
use App\Http\Controllers\Controller;
use App\Horario;
use Amranidev\Ajaxis\Ajaxis;
use URL;

/**
 * Class HorarioController
 *
 * @author  The scaffold-interface created at 2016-03-08 07:58:33pm
 * @link  https://github.com/amranidev/scaffold-interfac
 */
class HorarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $horarios = Horario::all();
        return view('horario.index',compact('horarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('horario.create'
                );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = Request::except('_token');

        $horario = new Horario();

        
        $horario->nombre = $input['nombre'];

        
        $horario->horainicio = $input['horainicio'];

        
        $horario->horafin = $input['horafin'];

        
        
        $horario->save();

        return redirect('horario');
    }

    /**
     * Display the specified resource.
     *
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Request::ajax())
        {
            return URL::to('horario/'.$id);
        }

        $horario = Horario::findOrfail($id);
        return view('horario.show',compact('horario'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Request::ajax())
        {
            return URL::to('horario/'. $id . '/edit');
        }

        
        $horario = Horario::findOrfail($id);
        return view('horario.edit',compact('horario'
                )
                );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id)
    {
        $input = Request::except('_token');

        $horario = Horario::findOrfail($id);
    	
        $horario->nombre = $input['nombre'];
        
        $horario->horainicio = $input['horainicio'];
        
        $horario->horafin = $input['horafin'];
        
        
        $horario->save();

        return redirect('horario');
    }

    /**
     * Delete confirmation message by Ajaxis
     *
     * @link  https://github.com/amranidev/ajaxis
     *
     * @return  String
     */
    public function DeleteMsg($id)
    {
        $msg = Ajaxis::MtDeleting('Warning!!','Would you like to remove This?','/horario/'. $id . '/delete/');

        if(Request::ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$horario = Horario::findOrfail($id);
     	$horario->delete();
        return URL::to('horario');
    }

}
