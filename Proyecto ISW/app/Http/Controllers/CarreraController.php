<?php
namespace App\Http\Controllers;

use Request;
use App\Http\Controllers\Controller;
use App\Carrera;
use Amranidev\Ajaxis\Ajaxis;
use URL;

/**
 * Class CarreraController
 *
 * @author  The scaffold-interface created at 2016-03-08 07:57:07pm
 * @link  https://github.com/amranidev/scaffold-interfac
 */
class CarreraController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $carreras = Carrera::all();
        return view('carrera.index',compact('carreras'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('carrera.create'
                );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = Request::except('_token');

        $carrera = new Carrera();

        
        $carrera->nombre = $input['nombre'];

        
        $carrera->codigo = $input['codigo'];

        
        
        $carrera->save();

        return redirect('carrera');
    }

    /**
     * Display the specified resource.
     *
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Request::ajax())
        {
            return URL::to('carrera/'.$id);
        }

        $carrera = Carrera::findOrfail($id);
        return view('carrera.show',compact('carrera'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Request::ajax())
        {
            return URL::to('carrera/'. $id . '/edit');
        }

        
        $carrera = Carrera::findOrfail($id);
        return view('carrera.edit',compact('carrera'
                )
                );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id)
    {
        $input = Request::except('_token');

        $carrera = Carrera::findOrfail($id);
    	
        $carrera->nombre = $input['nombre'];
        
        $carrera->codigo = $input['codigo'];
        
        
        $carrera->save();

        return redirect('carrera');
    }

    /**
     * Delete confirmation message by Ajaxis
     *
     * @link  https://github.com/amranidev/ajaxis
     *
     * @return  String
     */
    public function DeleteMsg($id)
    {
        $msg = Ajaxis::MtDeleting('Warning!!','Would you like to remove This?','/carrera/'. $id . '/delete/');

        if(Request::ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$carrera = Carrera::findOrfail($id);
     	$carrera->delete();
        return URL::to('carrera');
    }

}
