<?php
namespace App\Http\Controllers;

use Request;
use App\Http\Controllers\Controller;
use App\Recinto;
use Amranidev\Ajaxis\Ajaxis;
use URL;

/**
 * Class RecintoController
 *
 * @author  The scaffold-interface created at 2016-03-08 08:06:03pm
 * @link  https://github.com/amranidev/scaffold-interfac
 */
class RecintoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $recintos = Recinto::all();
        return view('recinto.index',compact('recintos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('recinto.create'
                );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = Request::except('_token');

        $recinto = new Recinto();

        
        $recinto->nombre = $input['nombre'];

        
        $recinto->direccion = $input['direccion'];

        
        $recinto->id_horario = $input['id_horario'];

        
        $recinto-> = $input[''];

        
        
        $recinto->save();

        return redirect('recinto');
    }

    /**
     * Display the specified resource.
     *
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Request::ajax())
        {
            return URL::to('recinto/'.$id);
        }

        $recinto = Recinto::findOrfail($id);
        return view('recinto.show',compact('recinto'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Request::ajax())
        {
            return URL::to('recinto/'. $id . '/edit');
        }

        
        $recinto = Recinto::findOrfail($id);
        return view('recinto.edit',compact('recinto'
                )
                );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id)
    {
        $input = Request::except('_token');

        $recinto = Recinto::findOrfail($id);
    	
        $recinto->nombre = $input['nombre'];
        
        $recinto->direccion = $input['direccion'];
        
        $recinto->id_horario = $input['id_horario'];
        
        $recinto-> = $input[''];
        
        
        $recinto->save();

        return redirect('recinto');
    }

    /**
     * Delete confirmation message by Ajaxis
     *
     * @link  https://github.com/amranidev/ajaxis
     *
     * @return  String
     */
    public function DeleteMsg($id)
    {
        $msg = Ajaxis::MtDeleting('Warning!!','Would you like to remove This?','/recinto/'. $id . '/delete/');

        if(Request::ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$recinto = Recinto::findOrfail($id);
     	$recinto->delete();
        return URL::to('recinto');
    }

}
