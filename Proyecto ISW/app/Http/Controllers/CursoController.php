<?php
namespace App\Http\Controllers;

use Request;
use App\Http\Controllers\Controller;
use App\Curso;
use Amranidev\Ajaxis\Ajaxis;
use URL;

/**
 * Class CursoController
 *
 * @author  The scaffold-interface created at 2016-03-08 07:56:51pm
 * @link  https://github.com/amranidev/scaffold-interfac
 */
class CursoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $cursos = Curso::all();
        return view('curso.index',compact('cursos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('curso.create'
                );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = Request::except('_token');

        $curso = new Curso();

        
        $curso->nombre = $input['nombre'];

        
        $curso->codigo = $input['codigo'];

        
        
        $curso->save();

        return redirect('curso');
    }

    /**
     * Display the specified resource.
     *
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Request::ajax())
        {
            return URL::to('curso/'.$id);
        }

        $curso = Curso::findOrfail($id);
        return view('curso.show',compact('curso'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Request::ajax())
        {
            return URL::to('curso/'. $id . '/edit');
        }

        
        $curso = Curso::findOrfail($id);
        return view('curso.edit',compact('curso'
                )
                );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id)
    {
        $input = Request::except('_token');

        $curso = Curso::findOrfail($id);
    	
        $curso->nombre = $input['nombre'];
        
        $curso->codigo = $input['codigo'];
        
        
        $curso->save();

        return redirect('curso');
    }

    /**
     * Delete confirmation message by Ajaxis
     *
     * @link  https://github.com/amranidev/ajaxis
     *
     * @return  String
     */
    public function DeleteMsg($id)
    {
        $msg = Ajaxis::MtDeleting('Warning!!','Would you like to remove This?','/curso/'. $id . '/delete/');

        if(Request::ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$curso = Curso::findOrfail($id);
     	$curso->delete();
        return URL::to('curso');
    }

}
