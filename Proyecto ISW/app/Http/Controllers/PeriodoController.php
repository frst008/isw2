<?php
namespace App\Http\Controllers;

use Request;
use App\Http\Controllers\Controller;
use App\Periodo;
use Amranidev\Ajaxis\Ajaxis;
use URL;

/**
 * Class PeriodoController
 *
 * @author  The scaffold-interface created at 2016-03-08 07:57:41pm
 * @link  https://github.com/amranidev/scaffold-interfac
 */
class PeriodoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        $periodos = Periodo::all();
        return view('periodo.index',compact('periodos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('periodo.create'
                );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @return  \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = Request::except('_token');

        $periodo = new Periodo();

        
        $periodo->nombre = $input['nombre'];

        
        $periodo->fechainicio = $input['fechainicio'];

        
        $periodo->fechafin = $input['fechafin'];

        
        
        $periodo->save();

        return redirect('periodo');
    }

    /**
     * Display the specified resource.
     *
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(Request::ajax())
        {
            return URL::to('periodo/'.$id);
        }

        $periodo = Periodo::findOrfail($id);
        return view('periodo.show',compact('periodo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Request::ajax())
        {
            return URL::to('periodo/'. $id . '/edit');
        }

        
        $periodo = Periodo::findOrfail($id);
        return view('periodo.edit',compact('periodo'
                )
                );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param    \Illuminate\Http\Request  $request
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function update($id)
    {
        $input = Request::except('_token');

        $periodo = Periodo::findOrfail($id);
    	
        $periodo->nombre = $input['nombre'];
        
        $periodo->fechainicio = $input['fechainicio'];
        
        $periodo->fechafin = $input['fechafin'];
        
        
        $periodo->save();

        return redirect('periodo');
    }

    /**
     * Delete confirmation message by Ajaxis
     *
     * @link  https://github.com/amranidev/ajaxis
     *
     * @return  String
     */
    public function DeleteMsg($id)
    {
        $msg = Ajaxis::MtDeleting('Warning!!','Would you like to remove This?','/periodo/'. $id . '/delete/');

        if(Request::ajax())
        {
            return $msg;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param    int  $id
     * @return  \Illuminate\Http\Response
     */
    public function destroy($id)
    {
     	$periodo = Periodo::findOrfail($id);
     	$periodo->delete();
        return URL::to('periodo');
    }

}
