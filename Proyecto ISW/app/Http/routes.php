<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});

//Curso Resources
/*******************************************************/
Route::resource('curso','CursoController');
Route::post('curso/{id}/update','CursoController@update');
Route::get('curso/{id}/delete','CursoController@destroy');
Route::get('curso/{id}/deleteMsg','CursoController@DeleteMsg');
/********************************************************/

//Carrera Resources
/*******************************************************/
Route::resource('carrera','CarreraController');
Route::post('carrera/{id}/update','CarreraController@update');
Route::get('carrera/{id}/delete','CarreraController@destroy');
Route::get('carrera/{id}/deleteMsg','CarreraController@DeleteMsg');
/********************************************************/

//Periodo Resources
/*******************************************************/
Route::resource('periodo','PeriodoController');
Route::post('periodo/{id}/update','PeriodoController@update');
Route::get('periodo/{id}/delete','PeriodoController@destroy');
Route::get('periodo/{id}/deleteMsg','PeriodoController@DeleteMsg');
/********************************************************/

//Horario Resources
/*******************************************************/
Route::resource('horario','HorarioController');
Route::post('horario/{id}/update','HorarioController@update');
Route::get('horario/{id}/delete','HorarioController@destroy');
Route::get('horario/{id}/deleteMsg','HorarioController@DeleteMsg');
/********************************************************/

//Recinto Resources
/*******************************************************/
Route::resource('recinto','RecintoController');
Route::post('recinto/{id}/update','RecintoController@update');
Route::get('recinto/{id}/delete','RecintoController@destroy');
Route::get('recinto/{id}/deleteMsg','RecintoController@DeleteMsg');
/********************************************************/
