<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class CursoController
 *
 * @author  The scaffold-interface created at 2016-03-08 07:56:51pm
 * @link  https://github.com/amranidev/scaffold-interfac
 */
class Curso extends Model
{

    public $timestamps = false;

    protected $table = 'cursos';

	
}
