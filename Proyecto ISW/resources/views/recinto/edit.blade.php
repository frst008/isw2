<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.1/css/materialize.min.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <title>Edit Recinto</title>
    </head>
    <body>
        <div class = 'container'>
            <h1>Edit Recinto</h1>
            <form method = 'get' action = 'http://localhost:8000/recinto'>
                <button class = 'btn blue'>Recinto Index</button>
            </form>
            <br>
            <form method = 'POST' action = 'http://localhost:8000/recinto/{{$recinto->id}}/update'>
                <input type = 'hidden' name = '_token' value = '{{Session::token()}}'>
                
                <div class="input-field col s6">
                    <input id="nombre" name = "nombre" type="text" class="validate" value="{{$recinto->nombre}}">
                    <label for="nombre">nombre</label>
                </div>
                
                <div class="input-field col s6">
                    <input id="direccion" name = "direccion" type="text" class="validate" value="{{$recinto->direccion}}">
                    <label for="direccion">direccion</label>
                </div>
                
                <div class="input-field col s6">
                    <input id="id_horario" name = "id_horario" type="text" class="validate" value="{{$recinto->id_horario}}">
                    <label for="id_horario">id_horario</label>
                </div>
                
                <div class="input-field col s6">
                    <input id="" name = "" type="text" class="validate" value="{{$recinto->}}">
                    <label for=""></label>
                </div>
                                
                <button class = 'btn red' type ='submit'>Update</button>
            </form>
        </div>
    </body>
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.1/js/materialize.min.js"></script>
    <script type="text/javascript">
    $('select').material_select();
    </script>
</html>
