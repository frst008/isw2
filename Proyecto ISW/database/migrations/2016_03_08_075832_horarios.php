<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Horarios
 *
 * @author  The scaffold-interface created at 2016-03-08 07:58:32pm
 * @link  https://github.com/amranidev/scaffold-interfac
 */
class Horarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('horarios',function (Blueprint $table){

        $table->increments('id');
        
        $table->string('nombre');
        
        $table->date('horainicio');
        
        $table->date('horafin');
        
        /**
         * Foreignkeys section
         */
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('horarios');
     }
}
