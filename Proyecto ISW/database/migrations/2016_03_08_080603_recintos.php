<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Recintos
 *
 * @author  The scaffold-interface created at 2016-03-08 08:06:03pm
 * @link  https://github.com/amranidev/scaffold-interfac
 */
class Recintos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('recintos',function (Blueprint $table){

        $table->increments('id');
        
        $table->string('nombre');
        
        $table->string('direccion');
        
        $table->integer('id_horario');
        
        $table->horarios('');
        
        /**
         * Foreignkeys section
         */
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('recintos');
     }
}
