<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Periodos
 *
 * @author  The scaffold-interface created at 2016-03-08 07:57:41pm
 * @link  https://github.com/amranidev/scaffold-interfac
 */
class Periodos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('periodos',function (Blueprint $table){

        $table->increments('id');
        
        $table->string('nombre');
        
        $table->date('fechainicio');
        
        $table->date('fechafin');
        
        /**
         * Foreignkeys section
         */
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('periodos');
     }
}
