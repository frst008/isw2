<?php
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * Class Carreras
 *
 * @author  The scaffold-interface created at 2016-03-08 07:57:07pm
 * @link  https://github.com/amranidev/scaffold-interfac
 */
class Carreras extends Migration
{
    /**
     * Run the migrations.
     *
     * @return  void
     */
    public function up()
    {
        Schema::create('carreras',function (Blueprint $table){

        $table->increments('id');
        
        $table->string('nombre');
        
        $table->string('codigo');
        
        /**
         * Foreignkeys section
         */
        
        // type your addition here

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return  void
     */
    public function down()
    {
        Schema::drop('carreras');
     }
}
